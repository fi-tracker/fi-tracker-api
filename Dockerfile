FROM openjdk:11
VOLUME /tmp
COPY target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]

# docker build -t ohapps/fi-tracker .

# docker run -p 8080:8080 ohapps/fi-tracker