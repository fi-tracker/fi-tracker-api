package com.ohapps.fitracker.constant;

public enum TestUser {

    TESTUSER("craig@ohapps.com", "S$cretPassw0rd", 1),
    TESTUSERUPDATE("testupdate@ohapps.com", "Gn5pR$AB39c%", 1),
    TESTUSER2("test2@ohapps.com", "z2VY7lR0B#%&", 1),
    NEWUSER("new@ohapps.com", "3!!dJ7%EyacX", 1);

    private final String email;
    private final String password;
    private final int active;

    TestUser(String email, String password, int active) {
        this.email = email;
        this.password = password;
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getActive() {
        return active;
    }
}
