package com.ohapps.fitracker.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.AccountType;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.security.TokenAuthenticationService;
import com.ohapps.fitracker.service.InvestmentService;
import com.ohapps.fitracker.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class InvestmentControllerTest extends BaseUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserService userService;

    @Autowired
    private InvestmentService investmentService;

    @Test
    public void testGetAccountTypes() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        MvcResult result = mockMvc.perform(get("/investment/account-types")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        assertFalse(StringUtils.isEmpty(result.getResponse().getContentAsString()));
    }

    @Test
    public void testGetInvestments() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        MvcResult result = mockMvc.perform(get("/investment")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        List<Investment> investmentList = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Investment>>(){});
        assertFalse(CollectionUtils.isEmpty(investmentList));
    }

    @Test
    public void testSaveInvestment() throws Exception {

        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        Investment investment = new Investment(
                testUser.getId(),
                "single family rental",
                "real estate",
                AccountType.INVESTMENT.getType(),
                BigDecimal.valueOf(50000),
                BigDecimal.valueOf(40000),
                BigDecimal.valueOf(6000));

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        MvcResult result = mockMvc.perform(post("/investment")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(investment)))
                .andExpect(status().isOk())
                .andReturn();

        Investment investmentReturned = mapper.readValue(result.getResponse().getContentAsString(), Investment.class);
        assertNotNull(investmentReturned.getId());
    }

    @Test
    public void testDeleteInvestment() throws Exception {

        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));

        MvcResult result = mockMvc.perform(delete("/investment/" + investment1.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<Investment> updatedInvestments = investmentService.findByUserId(testUser.getId());
        assertNotNull(updatedInvestments);
        assertNotEquals(investments.size(), updatedInvestments.size());
    }
}
