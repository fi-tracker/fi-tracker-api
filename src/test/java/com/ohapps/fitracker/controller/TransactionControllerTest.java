package com.ohapps.fitracker.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.security.TokenAuthenticationService;
import com.ohapps.fitracker.util.RestPageImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TransactionControllerTest extends BaseUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testGetTransactions() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        MvcResult result = mockMvc.perform(get("/transaction/" + this.investment1.getId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        RestPageImpl<Transaction> transactions = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<RestPageImpl<Transaction>>(){});
        assertFalse(CollectionUtils.isEmpty(transactions.getContent()));
    }

    @Test
    public void testSaveNewTransaction() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        Transaction transaction = new Transaction();
        transaction.setInvestmentId(this.investment1.getId());
        transaction.setAmount(BigDecimal.TEN);
        transaction.setDescription("test");
        transaction.setTransactionDate(LocalDate.now());
        transaction.setType(TransactionType.GAIN.getType());

        MvcResult result = mockMvc.perform(post("/transaction")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(transaction)))
                .andExpect(status().isOk())
                .andReturn();

        Transaction transactionReturned = mapper.readValue(result.getResponse().getContentAsString(), Transaction.class);
        assertNotNull(transactionReturned.getId());
    }

    @Test
    public void testUpdateExistingTransaction() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        this.transaction1.setDescription("updated transaction");
        this.transaction1.setAmount(BigDecimal.valueOf(200));

        MvcResult result = mockMvc.perform(post("/transaction")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(this.transaction1)))
                .andExpect(status().isOk())
                .andReturn();

        Transaction transactionReturned = mapper.readValue(result.getResponse().getContentAsString(), Transaction.class);
        assertNotNull(transactionReturned.getId());
        assertEquals("updated transaction", transactionReturned.getDescription());
        assertEquals(BigDecimal.valueOf(200), transactionReturned.getAmount());
    }

    @Test
    public void testSaveNewTransactionFailedAccessDenied() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER2.getEmail());
        assertNotNull(token);

        Transaction transaction = new Transaction();
        transaction.setInvestmentId(this.investment1.getId());
        transaction.setAmount(BigDecimal.TEN);
        transaction.setDescription("test");
        transaction.setTransactionDate(LocalDate.now());
        transaction.setType(TransactionType.GAIN.getType());

        mockMvc.perform(post("/transaction")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(transaction)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void testDeleteTransaction() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        mockMvc.perform(delete("/transaction/" + this.transaction1.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testDeleteTransactionFailedAccessDenied() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER2.getEmail());
        assertNotNull(token);

        mockMvc.perform(delete("/transaction/" + this.transaction1.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}
