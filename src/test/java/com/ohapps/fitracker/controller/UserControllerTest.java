package com.ohapps.fitracker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.dto.AuthDto;
import com.ohapps.fitracker.dto.ModifyUserDto;
import com.ohapps.fitracker.dto.UserDto;
import com.ohapps.fitracker.security.TokenAuthenticationService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserControllerTest extends BaseUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testRegisterUser() throws Exception {

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.NEWUSER.getEmail());
        modifyUserDto.setPassword(TestUser.NEWUSER.getPassword());

        mockMvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testRegisterUserEmailExists() throws Exception {

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.TESTUSER.getEmail());
        modifyUserDto.setPassword(TestUser.TESTUSER.getPassword());

        mockMvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("email is already taken"))
                .andReturn();
    }

    @Test
    public void testRegisterUserWeakPassword() throws Exception {

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.NEWUSER.getEmail());
        modifyUserDto.setPassword("123456");

        mockMvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("invalid user password"))
                .andReturn();
    }

    @Test
    public void testUserAuth() throws Exception {

        mockMvc.perform(post("/user/auth")
                .param("username", TestUser.TESTUSER.getEmail())
                .param("password", TestUser.TESTUSER.getPassword())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testUserAuthFailed() throws Exception {

        mockMvc.perform(post("/user/auth")
                .param("username", TestUser.TESTUSER.getEmail())
                .param("password", "123456789")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void testUserInfo() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        MvcResult result = mockMvc.perform(get("/user/info")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        UserDto userDto = mapper.readValue(result.getResponse().getContentAsString(), UserDto.class);
        assertEquals(userDto.getEmail(), TestUser.TESTUSER.getEmail());
    }

    @Test
    public void testUserInfoFailed() throws Exception {

        mockMvc.perform(get("/user/info"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testUpdateExistingUserPassword() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.TESTUSER.getEmail());
        modifyUserDto.setPassword("z2VY7lR0B#%&");
        modifyUserDto.setMonthlyExpenses(BigDecimal.valueOf(5000));
        modifyUserDto.setTargetIncome(BigDecimal.valueOf(1000));

        MvcResult result = mockMvc.perform(post("/user/save")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isOk())
                .andReturn();

        AuthDto authDto = mapper.readValue(result.getResponse().getContentAsString(), AuthDto.class);
        assertNotNull(authDto.getToken());
        assertNotNull(authDto.getExpiration());
    }

    @Test
    public void testUpdateExistingUserEmail() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.NEWUSER.getEmail());

        MvcResult result = mockMvc.perform(post("/user/save")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isOk())
                .andReturn();

        AuthDto authDto = mapper.readValue(result.getResponse().getContentAsString(), AuthDto.class);
        assertNotNull(authDto.getToken());
        assertNotNull(authDto.getExpiration());
    }

    @Test
    public void testUpdateExistingUserEmailAlreadyTaken() throws Exception {

        String token = TokenAuthenticationService.createToken(TestUser.TESTUSER.getEmail());
        assertNotNull(token);

        ModifyUserDto modifyUserDto = new ModifyUserDto();
        modifyUserDto.setEmail(TestUser.TESTUSER2.getEmail());

        mockMvc.perform(post("/user/save")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(modifyUserDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("email is already taken"))
                .andReturn();
    }
}
