package com.ohapps.fitracker.dao;

import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.portfolio.MonthlySummary;
import com.ohapps.fitracker.util.InvestmentUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles({"dev"})
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Ignore
public class PortfolioDaoIT {

    @Autowired
    private PortfolioDao portfolioDao;

    @Test
    public void testGetPassiveIncome() {

        List<String> investmentIds = List.of("5df06de35b936b6c025cb9c9");
        LocalDate now = LocalDate.now();
        LocalDateTime endDate = LocalDateTime.of(LocalDate.of(now.getYear(), now.getMonth(), 1), LocalTime.of(0, 0));
        LocalDateTime startDate = endDate.minusYears(1);
        List<String> transactionTypes = Arrays.asList(TransactionType.GAIN.getType(), TransactionType.GAIN_REINVESTED.getType());

        List<MonthlySummary> results = portfolioDao.getPassiveIncome(investmentIds, startDate, endDate, transactionTypes);

        assertThat(results).isNotEmpty();
        assertThat(results.size()).isEqualTo(12);
    }

    @Test
    public void testGetTransactionTotal() {

        LocalDate now = LocalDate.now();
        LocalDateTime endDate = LocalDateTime.of(LocalDate.of(now.getYear(), now.getMonth(), 1), LocalTime.of(0, 0));
        LocalDateTime startDate = endDate.minusYears(1);
        BigDecimal results = portfolioDao.getTransactionTotal("5df06de35b936b6c025cb9c9", startDate, endDate, InvestmentUtils.getPassiveIncomeTypes());

        assertThat(results).isNotNull();
        assertThat(results).isGreaterThan(BigDecimal.ZERO);
    }

    @Test
    public void testGetTransactionTotalNotFound() {

        LocalDate now = LocalDate.now();
        LocalDateTime endDate = LocalDateTime.of(LocalDate.of(now.getYear(), now.getMonth(), 1), LocalTime.of(0, 0));
        LocalDateTime startDate = endDate.minusYears(1);
        BigDecimal results = portfolioDao.getTransactionTotal("1234", startDate, endDate, InvestmentUtils.getPassiveIncomeTypes());

        assertThat(results).isNotNull();
        assertThat(results).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    public void testGetOldestTransaction() {

        LocalDate now = LocalDate.now();
        LocalDateTime endDate = LocalDateTime.of(LocalDate.of(now.getYear(), now.getMonth(), 1), LocalTime.of(0, 0));
        LocalDateTime startDate = endDate.minusYears(1);

        Optional<Transaction> transaction = portfolioDao.getOldestTransaction("5e365bb78daf535b5019046b", startDate, endDate, Collections.singletonList(TransactionType.CHANGE_IN_VALUE.getType()));

        assertThat(transaction.isPresent()).isTrue();
        assertThat(transaction.get().getAmount()).isEqualTo(BigDecimal.valueOf(15000));
    }
}
