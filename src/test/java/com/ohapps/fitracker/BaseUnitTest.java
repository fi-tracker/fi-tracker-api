package com.ohapps.fitracker;

import com.ohapps.fitracker.constant.AccountType;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertTrue;

@ActiveProfiles({"test"})
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BaseUnitTest {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    protected User testUser;
    protected User testUser2;
    protected Investment investment1;
    protected Investment investment2;
    protected Investment investment3;
    protected Investment investment4;
    protected Investment investment5;
    protected Transaction transaction1;
    protected Transaction transaction2;
    protected Transaction transaction3;
    protected Transaction transaction4;
    protected Transaction transaction5;

    @Before
    public void setupDatabase() {
        // ADD TEST USERS
        testUser = new User(TestUser.TESTUSER.getEmail(), bCryptPasswordEncoder.encode(TestUser.TESTUSER.getPassword()), TestUser.TESTUSER.getActive());
        testUser.setMonthlyExpenses(BigDecimal.valueOf(5000));
        testUser.setTargetIncome(BigDecimal.valueOf(1000));
        testUser2 = new User(TestUser.TESTUSER2.getEmail(), bCryptPasswordEncoder.encode(TestUser.TESTUSER2.getPassword()), TestUser.TESTUSER2.getActive());
        mongoTemplate.insert(testUser, "users");
        mongoTemplate.insert(testUser2, "users");

        // ADD INVESTMENTS
        investment1 = new Investment(testUser.getId(), "us index fund", "mutual fund", AccountType.RETIREMENT.getType(), BigDecimal.valueOf(2000), BigDecimal.ZERO, BigDecimal.valueOf(3000));
        investment1.setInserted(LocalDateTime.now());
        mongoTemplate.insert(investment1, "investments");

        investment2 = new Investment(testUser2.getId(), "us index fund", "mutual fund", AccountType.RETIREMENT.getType(), BigDecimal.valueOf(2000), BigDecimal.ZERO, BigDecimal.valueOf(3000));
        investment2.setInserted(LocalDateTime.now());
        mongoTemplate.insert(investment2, "investments");

        investment3 = new Investment(testUser.getId(), "rental property", "real estate", AccountType.INVESTMENT.getType(), BigDecimal.valueOf(70000), BigDecimal.valueOf(50000), BigDecimal.valueOf(100000));
        investment3.setInserted(LocalDateTime.now());
        mongoTemplate.insert(investment3, "investments");

        investment4 = new Investment(testUser.getId(), "personal savings account", "savings account", AccountType.CASHRESERVE.getType(), BigDecimal.valueOf(70000), BigDecimal.ZERO, BigDecimal.valueOf(80000));
        investment4.setInserted(LocalDateTime.now());
        mongoTemplate.insert(investment4, "investments");

        investment5 = new Investment(testUser.getId(), "foreign index fund", "mutual fund", AccountType.RETIREMENT.getType(), BigDecimal.valueOf(500), BigDecimal.ZERO, BigDecimal.valueOf(1000));
        investment5.setInserted(LocalDateTime.now());
        mongoTemplate.insert(investment5, "investments");

        // ADD TRANSACTIONS
        transaction1 = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.GAIN_REINVESTED.getType(), "dividend", BigDecimal.valueOf(100));
        mongoTemplate.insert(transaction1, "transactions");

        transaction2 = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.PURCHASE_OF_INVESTMENT.getType(), "purchase more investment", BigDecimal.valueOf(500));
        mongoTemplate.insert(transaction2, "transactions");

        transaction3 = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.SALE_OF_INVESTMENT.getType(), "sale of investment", BigDecimal.valueOf(1000));
        mongoTemplate.insert(transaction3, "transactions");

        transaction4 = new Transaction(investment3.getId(), LocalDate.now(), TransactionType.DEBT_REPAYMENT.getType(), "pay down debt", BigDecimal.valueOf(5000));
        mongoTemplate.insert(transaction4, "transactions");

        transaction5 = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS_WITHDRAWN.getType(), "loss from sale of investment", BigDecimal.valueOf(-500));
        mongoTemplate.insert(transaction5, "transactions");

        // YEARLY RENTAL INCOME
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now(), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(100)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(1), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(100)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(2), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(111)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(3), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(125)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(4), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(115)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(5), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(150)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(6), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(128)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(7), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(189)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(8), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(147)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(9), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(133)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(10), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(118)), "transactions");
        mongoTemplate.insert(new Transaction(investment3.getId(), LocalDate.now().minusMonths(11), TransactionType.GAIN.getType(), "rental income", BigDecimal.valueOf(105)), "transactions");

        mongoTemplate.insert(new Transaction(investment1.getId(), LocalDate.now().minusMonths(11), TransactionType.CHANGE_IN_VALUE.getType(), "original value", BigDecimal.valueOf(10000)), "transactions");
        mongoTemplate.insert(new Transaction(investment1.getId(), LocalDate.now().minusMonths(1), TransactionType.CHANGE_IN_VALUE.getType(), "current value", BigDecimal.valueOf(15000)), "transactions");
    }

    @After
    public void resetDatabase() {
        mongoTemplate.dropCollection("users");
        mongoTemplate.dropCollection("investments");
        mongoTemplate.dropCollection("transactions");
    }

    @Test
    public void baseSetupTest() {
        assertTrue(true);
    }
}
