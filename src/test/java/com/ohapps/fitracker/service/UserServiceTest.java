package com.ohapps.fitracker.service;

import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class UserServiceTest extends BaseUnitTest {

    @Autowired
    UserService userService;

    @Test
    public void testFindByEmail() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);
        assertEquals(TestUser.TESTUSER.getEmail(), testUser.getEmail());
    }

    @Test
    public void testFindByEmailNotFound() {
        User testUser = userService.findByEmail(TestUser.NEWUSER.getEmail());
        assertNull(testUser);
    }
}
