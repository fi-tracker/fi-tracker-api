package com.ohapps.fitracker.service;

import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.dao.PortfolioDao;
import com.ohapps.fitracker.entity.UserPortfolio;
import com.ohapps.fitracker.entity.portfolio.MonthlySummary;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class PortfolioServiceTest extends BaseUnitTest {

    @Mock
    private PortfolioDao portfolioDao;

    @Autowired
    private InvestmentService investmentService;

    private PortfolioService portfolioService;

    @Before
    public void setUp() throws Exception {
        portfolioService = new PortfolioServiceImpl(investmentService, portfolioDao);
    }

    @Test
    public void testGetUserPortfolio() {

        List<MonthlySummary> monthlySummaries = new ArrayList<>();
        monthlySummaries.add(new MonthlySummary(LocalDate.now().getYear(), LocalDate.now().getMonth().getValue(), BigDecimal.valueOf(500)));
        when(portfolioDao.getPassiveIncome(anyList(), any(LocalDateTime.class), any(LocalDateTime.class), anyList())).thenReturn(monthlySummaries);
        when(portfolioDao.getTransactionTotal(anyString(), any(LocalDateTime.class), any(LocalDateTime.class), anyList())).thenReturn(BigDecimal.ZERO);

        UserPortfolio userPortfolio = portfolioService.getUserPortfolio(testUser, true);

        assertNotNull(userPortfolio);
        assertNotNull(userPortfolio.getCashReserves());
        assertNotNull(userPortfolio.getPortfolioDebt());
        assertNotNull(userPortfolio.getPortfolioValue());
        assertNotNull(userPortfolio.getMonthsReserve());
        assertFalse(userPortfolio.getAccountTypeBalances().isEmpty());
        assertFalse(userPortfolio.getInvestmentTypeBalances().isEmpty());
        assertFalse(userPortfolio.getPassiveIncome().isEmpty());
        assertFalse(userPortfolio.getInvestmentSummaries().isEmpty());
    }
}
