package com.ohapps.fitracker.service;

import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.AccountType;
import com.ohapps.fitracker.constant.TestUser;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class InvestmentServiceTest extends BaseUnitTest {

    @Autowired
    private UserService userService;

    @Autowired
    private InvestmentService investmentService;

    @Test
    public void testFindByUserId() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));
    }

    @Test
    public void testFindById() {
        Investment investment = investmentService.findById(this.investment1.getId());
        assertNotNull(investment);
        assertEquals(this.investment1.getId(), investment.getId());
    }

    @Test
    public void testSaveNewInvestment() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        Investment investment = new Investment(testUser.getId(), "single family rental", "real estate", AccountType.INVESTMENT.getType(), BigDecimal.valueOf(50000), BigDecimal.valueOf(40000), BigDecimal.valueOf(6000));
        investmentService.save(investment);
        assertNotNull(investment.getId());
        assertNotNull(investment.getInserted());
    }

    @Test
    public void testUpdateInvestment() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));
        Investment investment = investments.get(0);
        investment.setDescription("updated investment");

        assertNull(investment.getUpdated());
        investmentService.save(investment);
        assertNotNull(investment.getUpdated());
        assertEquals("updated investment", investment.getDescription());
    }

    @Test
    public void testDeleteAllInvestments() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));

        for(Investment investment : investments) {
            investmentService.delete(investment);
        }

        investments = investmentService.findByUserId(testUser.getId());
        assertTrue(CollectionUtils.isEmpty(investments));
    }

    @Test
    public void testIsUsersInvestment() {
        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));

        for(Investment investment : investments) {
            assertTrue(investmentService.isUsersInvestment(investment, testUser.getId()));
        }

        // test overwrite userId with current user for new investments
        Investment newInvestment = new Investment("123123123123", "single family rental", "real estate", AccountType.INVESTMENT.getType(), BigDecimal.valueOf(50000), BigDecimal.valueOf(40000), BigDecimal.valueOf(6000));
        assertTrue(investmentService.isUsersInvestment(newInvestment, testUser.getId()));

        User testUser2 = userService.findByEmail(TestUser.TESTUSER2.getEmail());
        assertNotNull(testUser2);

        List<Investment> otherUserInvestments = investmentService.findByUserId(testUser2.getId());
        assertFalse(CollectionUtils.isEmpty(otherUserInvestments));

        for(Investment investment : otherUserInvestments) {
            assertFalse(investmentService.isUsersInvestment(investment, testUser.getId()));
        }
    }

    @Test
    public void testPreventUserSpoofInIsUsersInvestment() {

        User testUser = userService.findByEmail(TestUser.TESTUSER.getEmail());
        assertNotNull(testUser);

        List<Investment> investments = investmentService.findByUserId(testUser.getId());
        assertFalse(CollectionUtils.isEmpty(investments));

        Investment investment = investments.get(0);

        investment.setUserId("123123123");
        assertFalse(investmentService.isUsersInvestment(investment, "123123123"));
    }
}
