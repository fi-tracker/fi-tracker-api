package com.ohapps.fitracker.service;

import com.ohapps.fitracker.BaseUnitTest;
import com.ohapps.fitracker.constant.ErrorCode;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.TransactionUpdate;
import io.jsonwebtoken.lang.Collections;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class TransactionServiceTest extends BaseUnitTest {

    @Autowired
    InvestmentService investmetService;

    @Autowired
    TransactionService transactionService;

    @Test
    public void testGetTypes() {
        List<String> types = transactionService.transactionTypes();
        assertNotNull(types);
        assertFalse(CollectionUtils.isEmpty(types));
    }

    @Test
    public void testGetTransactions() {
        Page<Transaction> transactions = transactionService.findByInvestmentId(this.investment1.getId(), 20);
        assertFalse(CollectionUtils.isEmpty(transactions.getContent()));
    }

    @Test
    public void testGetTransactionsNotFound() {
        Page<Transaction> transactions = transactionService.findByInvestmentId(this.investment2.getId(), 20);
        assertTrue(CollectionUtils.isEmpty(transactions.getContent()));
    }

    @Test
    public void testSaveNewTransaction() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS.getType(), "holding fees", BigDecimal.valueOf(50));
        Transaction saveTransaction = transactionService.save(transaction, false);
        assertNotNull(saveTransaction.getId());
    }

    @Test
    public void testUpdateExistingTransaction() {
        assertNotEquals("updated transaction", this.transaction1.getDescription());
        assertNotEquals(BigDecimal.valueOf(200), this.transaction1.getAmount());

        this.transaction1.setDescription("updated transaction");
        this.transaction1.setAmount(BigDecimal.valueOf(200));
        Transaction updatedTransaction = transactionService.save(this.transaction1, false);

        assertNotNull(updatedTransaction);
        assertEquals("updated transaction", updatedTransaction.getDescription());
        assertEquals( BigDecimal.valueOf(200), updatedTransaction.getAmount());
    }

    @Test
    public void testSaveForUserId() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS.getType(), "holding fees", BigDecimal.valueOf(50));
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());
        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());
    }

    @Test
    public void testUpdateForUserId() {
        this.transaction1.setDescription("updated transaction");
        this.transaction1.setAmount(BigDecimal.valueOf(200));
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(this.transaction1, testUser.getId());
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertEquals(BigDecimal.valueOf(200), transaction1.getAmount());
    }

    @Test
    public void testSaveForUserIdInvalidInvestmentId() {
        Transaction transaction = new Transaction("1231231231231", LocalDate.now(), TransactionType.LOSS.getType(), "holding fees", BigDecimal.valueOf(50));
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());
        assertNotNull(transactionUpdate);
        assertFalse(transactionUpdate.isSuccess());
        assertFalse(Collections.isEmpty(transactionUpdate.getErrors()));
        assertEquals(ErrorCode.INVALID_INVESTMENT_ID.getCode(), transactionUpdate.getErrors().get(0).getCode());
    }

    @Test
    public void testSaveForUserIdAccessDenied() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS.getType(), "holding fees", BigDecimal.valueOf(50));
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser2.getId());
        assertNotNull(transactionUpdate);
        assertFalse(transactionUpdate.isSuccess());
        assertFalse(Collections.isEmpty(transactionUpdate.getErrors()));
        assertEquals(ErrorCode.ACCESS_DENIED.getCode(), transactionUpdate.getErrors().get(0).getCode());
    }

    @Test
    public void testSaveForUserIdInvalidTransactionType() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), "test", "holding fees", BigDecimal.valueOf(50));
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());
        assertNotNull(transactionUpdate);
        assertFalse(transactionUpdate.isSuccess());
        assertFalse(Collections.isEmpty(transactionUpdate.getErrors()));
        assertEquals(ErrorCode.INVALID_TRANSACTION_TYPE.getCode(), transactionUpdate.getErrors().get(0).getCode());
    }

    @Test
    public void testDeleteTransaction() {
        Page<Transaction> transactions = transactionService.findByInvestmentId(this.investment1.getId(), 20);
        assertFalse(CollectionUtils.isEmpty(transactions.getContent()));
        for (Transaction transaction : transactions) {
            transactionService.delete(transaction, false);
        }
        transactions = transactionService.findByInvestmentId(this.investment1.getId(), 20);
        assertTrue(CollectionUtils.isEmpty(transactions.getContent()));
    }

    @Test
    public void testAddTransactionGainReinvested() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.GAIN_REINVESTED.getType(), "dividend reinvested", BigDecimal.valueOf(100));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(BigDecimal.valueOf(100)), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testAddTransactionLossAndVerifyNegativeAmount() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS.getType(), "loss in value", BigDecimal.valueOf(500));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());
        assertEquals(BigDecimal.valueOf(-500), transactionUpdate.getTransaction().getAmount());
    }

    @Test
    public void testAddTransactionLossWithdrawnAndVerifyNegativeAmount() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.LOSS_WITHDRAWN.getType(), "loss deducted from account", BigDecimal.valueOf(500));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());
        assertEquals(BigDecimal.valueOf(-500), transactionUpdate.getTransaction().getAmount());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(BigDecimal.valueOf(500)), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testAddTransactionPurchaseOfInvestment() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.PURCHASE_OF_INVESTMENT.getType(), "additional investment", BigDecimal.valueOf(500));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(BigDecimal.valueOf(500)), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().add(BigDecimal.valueOf(500)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testAddTransactionSaleOfInvestment() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.SALE_OF_INVESTMENT.getType(), "sale of investment", BigDecimal.valueOf(500));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(BigDecimal.valueOf(500)), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().subtract(BigDecimal.valueOf(500)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testAddTransactionChangeInValue() {
        Transaction transaction = new Transaction(investment1.getId(), LocalDate.now(), TransactionType.CHANGE_IN_VALUE.getType(), "change in value", BigDecimal.valueOf(5800));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(BigDecimal.valueOf(5800), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testAddTransactionDebtRepayment() {
        Transaction transaction = new Transaction(investment3.getId(), LocalDate.now(), TransactionType.DEBT_REPAYMENT.getType(), "pay down debt", BigDecimal.valueOf(5000));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment3.getId());
        assertEquals(investment3.getCurrentDebt().subtract(BigDecimal.valueOf(5000)), updatedInvestment.getCurrentDebt());
        assertEquals(investment3.getCostBasis().add(BigDecimal.valueOf(5000)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testModifyTransactionGainReinvested() {
        // change amount from $100 to $150 which should increase current value only by $50
        transaction1.setAmount(BigDecimal.valueOf(150));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction1, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(BigDecimal.valueOf(50)), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testModifyTransactionLossWithdrawn() {
        // change amount from $100 to $150 which should increase current value only by $50
        transaction5.setAmount(BigDecimal.valueOf(150));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction5, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(BigDecimal.valueOf(350)), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testModifyTransactionPurchaseOfInvestment() {

        transaction2.setAmount(BigDecimal.valueOf(400));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction2, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(BigDecimal.valueOf(100)), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().subtract(BigDecimal.valueOf(100)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testModifyTransactionSaleOfInvestment() {

        transaction3.setAmount(BigDecimal.valueOf(500));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction3, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(BigDecimal.valueOf(500)), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().add(BigDecimal.valueOf(500)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testModifyTransactionDebtRepayment() {

        transaction4.setAmount(BigDecimal.valueOf(10000));

        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transaction4, testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());
        assertNotNull(transactionUpdate.getTransaction());
        assertNotNull(transactionUpdate.getTransaction().getId());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment3.getId());
        assertEquals(investment3.getCurrentDebt().subtract(BigDecimal.valueOf(5000)), updatedInvestment.getCurrentDebt());
        assertEquals(investment3.getCostBasis().add(BigDecimal.valueOf(5000)), updatedInvestment.getCostBasis());
    }

    @Test
    public void testDeleteTransactionGainReinvested() {

        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(transaction1.getId(), testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(transaction1.getAmount()), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testDeleteTransactionLossWithdrawn() {

        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(transaction5.getId(), testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(transaction5.getAmount()), updatedInvestment.getCurrentValue());
    }

    @Test
    public void testDeleteTransactionPurchaseOfInvestment() {

        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(transaction2.getId(), testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().subtract(transaction2.getAmount()), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().subtract(transaction2.getAmount()), updatedInvestment.getCostBasis());
    }

    @Test
    public void testDeleteTransactionSaleOfInvestment() {

        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(transaction3.getId(), testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment1.getId());
        assertEquals(investment1.getCurrentValue().add(transaction3.getAmount()), updatedInvestment.getCurrentValue());
        assertEquals(investment1.getCostBasis().add(transaction3.getAmount()), updatedInvestment.getCostBasis());
    }

    @Test
    public void testDeleteTransactionDebtRepayment() {

        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(transaction4.getId(), testUser.getId());

        assertNotNull(transactionUpdate);
        assertTrue(transactionUpdate.isSuccess());

        // verify auto update
        Investment updatedInvestment = investmetService.findById(investment3.getId());
        assertEquals(investment3.getCurrentDebt().add(transaction4.getAmount()), updatedInvestment.getCurrentDebt());
        assertEquals(investment3.getCostBasis().subtract(transaction4.getAmount()), updatedInvestment.getCostBasis());
    }
}
