package com.ohapps.fitracker.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ModifyAccountDto {
    @NotBlank
    private String description;
}
