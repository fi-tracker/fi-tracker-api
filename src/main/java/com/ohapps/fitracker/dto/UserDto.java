package com.ohapps.fitracker.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserDto {

    private String id;
    private String email;
    private BigDecimal monthlyExpenses;
    private BigDecimal targetIncome;
    private BigDecimal retirementWithdrawRate;

}
