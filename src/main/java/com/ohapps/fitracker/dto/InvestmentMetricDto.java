package com.ohapps.fitracker.dto;

import com.ohapps.fitracker.constant.MetricType;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class InvestmentMetricDto {
    public String description;
    public BigDecimal monthlyAverage;
    public BigDecimal yearlyTotal;
    public BigDecimal lifetimeTotal;
    public MetricType metricType;
}
