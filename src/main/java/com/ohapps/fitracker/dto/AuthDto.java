package com.ohapps.fitracker.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AuthDto {

    private String token;
    @JsonFormat(shape=JsonFormat.Shape.NUMBER)
    private Date expiration;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
