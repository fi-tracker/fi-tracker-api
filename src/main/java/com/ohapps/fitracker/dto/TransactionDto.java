package com.ohapps.fitracker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDto {

    @Id
    private String id;
    @NotNull
    private String investmentId;
    private LocalDate transactionDate;
    private String type;
    private String description;
    private BigDecimal amount;

}
