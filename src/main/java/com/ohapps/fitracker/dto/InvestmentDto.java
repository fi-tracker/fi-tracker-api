package com.ohapps.fitracker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentDto {

    @Id
    private String id;
    @NotNull
    private String userId;
    private String description;
    private String type;
    private String accountType;
    private BigDecimal costBasis;
    private BigDecimal currentDebt;
    private BigDecimal currentValue;
    private String accountId;

}
