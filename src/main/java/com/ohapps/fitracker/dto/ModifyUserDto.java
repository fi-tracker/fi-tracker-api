package com.ohapps.fitracker.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ModifyUserDto {

    private String email;
    private String password;
    private BigDecimal monthlyExpenses;
    private BigDecimal targetIncome;
    private BigDecimal retirementWithdrawRate;

}
