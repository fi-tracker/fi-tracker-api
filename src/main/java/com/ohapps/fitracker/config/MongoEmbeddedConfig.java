package com.ohapps.fitracker.config;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class MongoEmbeddedConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoEmbeddedConfig.class);

    @Bean(destroyMethod = "shutdown")
    public MongoServer mongoServer() {
        MongoServer mongoServer = new MongoServer(new MemoryBackend());
        mongoServer.bind(); // bind to random, available port
        return mongoServer;
    }

    @Bean(destroyMethod = "close")
    public MongoClient mongoClient(MongoServer mongoServer) {
        LOGGER.info("mongoClient() - using in-memory-mongo instance");
        return new MongoClient(new ServerAddress(mongoServer.getLocalAddress()));
    }
}
