package com.ohapps.fitracker.repository;

import com.ohapps.fitracker.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findByEmail(String email);

}
