package com.ohapps.fitracker.repository;

import com.ohapps.fitracker.entity.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends MongoRepository<Account, String> {
    List<Account> findByUserIdOrderByDescription(String userId);
    Optional<Account> findByIdAndUserId(String id, String userId);
}
