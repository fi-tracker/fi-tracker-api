package com.ohapps.fitracker.repository;

import com.ohapps.fitracker.entity.Investment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InvestmentRepository extends MongoRepository<Investment, String> {

    List<Investment> findByUserId(String userId);
    List<Investment> findByUserIdAndAccountId(String userId, String accountId);

}
