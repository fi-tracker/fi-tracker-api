package com.ohapps.fitracker.repository;

import com.ohapps.fitracker.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionRepository extends MongoRepository<Transaction, String> {

    Page<Transaction> findByInvestmentIdOrderByTransactionDateDesc(String investmentId, Pageable pageable);

}
