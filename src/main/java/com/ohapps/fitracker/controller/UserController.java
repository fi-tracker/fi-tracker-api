package com.ohapps.fitracker.controller;

import com.ohapps.fitracker.dto.AuthDto;
import com.ohapps.fitracker.dto.ModifyUserDto;
import com.ohapps.fitracker.dto.UserDto;
import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.exception.InvalidUserException;
import com.ohapps.fitracker.mapper.UserMapper;
import com.ohapps.fitracker.security.CurrentUserService;
import com.ohapps.fitracker.security.TokenAuthenticationService;
import com.ohapps.fitracker.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserMapper userMapper;

    @ApiOperation(value = "user authentication to retrieve authorization token in header")
    @PostMapping(value = "/auth", headers = "Content-Type=application/x-www-form-urlencoded")
    public ResponseEntity login(@RequestParam String username, @RequestParam String password) {
        // this method only exists for documentation on the swagger page on how to authenticate with the application
        // all requests to this method are intercepted by the JWT login filter
        log.debug("login successful for username: {}", username);
        return ResponseEntity.ok("success");
    }

    @ApiOperation(value = "get current user information")
    @GetMapping(value = "/info")
    public UserDto info(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request) {
        log.debug("request user info for token: {}", authorization);
        User user = currentUserService.getCurrentUser(request);
        log.debug("response of user info for user: {}", user);
        return userMapper.convertEntityToDto(user);
    }

    @ApiOperation(value = "register new user")
    @PostMapping(value = "/register")
    public ResponseEntity<UserDto> register(@RequestBody ModifyUserDto modifyUserDto) {
        log.debug("request new user registration for email: {}", modifyUserDto.getEmail());
        User user = userService.register(modifyUserDto.getEmail(), modifyUserDto.getPassword());
        log.debug("new user registration complete for email: {}", modifyUserDto.getEmail());
        return ResponseEntity.ok(userMapper.convertEntityToDto(user));
    }

    @ApiOperation(value = "update current user information")
    @PostMapping(value = "/save")
    public AuthDto save(@RequestHeader(value = "Authorization") String authorization, @RequestBody ModifyUserDto modifyUserDto, HttpServletRequest request) {
        log.debug("request user save for email: {}", modifyUserDto.getEmail());
        User user = currentUserService.getCurrentUser(request);
        if (user == null) {
            throw new InvalidUserException("current user not found");
        }
        userMapper.updateEntityFromDto(user, modifyUserDto);
        userService.save(user);
        log.debug("response of user save for user: {}", user);
        return TokenAuthenticationService.createAuth(user.getEmail());
    }
}
