package com.ohapps.fitracker.controller;

import com.ohapps.fitracker.dto.ModifyAccountDto;
import com.ohapps.fitracker.entity.Account;
import com.ohapps.fitracker.security.CurrentUserService;
import com.ohapps.fitracker.service.AccountService;
import com.ohapps.fitracker.service.InvestmentService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final InvestmentService investmentService;
    private final CurrentUserService currentUserService;

    @ApiOperation(value = "get user accounts")
    @GetMapping
    public ResponseEntity<List<Account>> getAccounts(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request) {
        log.info("getAccounts() - get user accounts");
        return getCurrentUserAccounts(request);
    }

    @ApiOperation(value = "create new account")
    @PostMapping
    public ResponseEntity<List<Account>> createAccount(@RequestHeader(value = "Authorization") String authorization,
                                                       @Valid @RequestBody ModifyAccountDto modifyAccountDto,
                                                       HttpServletRequest request) {
        log.info("createAccount() - create new account");
        accountService.createAccount(modifyAccountDto, currentUserService.getCurrentUser(request).getId());
        return getCurrentUserAccounts(request);
    }

    @ApiOperation(value = "update existing account")
    @PutMapping("/{id}")
    public ResponseEntity<List<Account>> updateAccount(@RequestHeader(value = "Authorization") String authorization,
                                                       @PathVariable String id,
                                                       @Valid @RequestBody ModifyAccountDto modifyAccountDto,
                                                       HttpServletRequest request) {
        log.info("updateAccount() - update existing account");
        accountService.updateAccount(modifyAccountDto, id, currentUserService.getCurrentUser(request).getId());
        return getCurrentUserAccounts(request);
    }

    @ApiOperation(value = "delete existing account")
    @DeleteMapping("/{id}")
    public ResponseEntity<List<Account>> deleteAccount(@RequestHeader(value = "Authorization") String authorization,
                                                       @PathVariable String id,
                                                       HttpServletRequest request) {
        log.info("updateAccount() - update existing account");
        accountService.deleteAccount(id, currentUserService.getCurrentUser(request).getId());
        investmentService.clearAccountIdForUserId(id, currentUserService.getCurrentUser(request).getId());
        return getCurrentUserAccounts(request);
    }

    private ResponseEntity<List<Account>> getCurrentUserAccounts(HttpServletRequest request) {
        return ResponseEntity.ok(accountService.findByUserId(currentUserService.getCurrentUser(request).getId()));
    }
}
