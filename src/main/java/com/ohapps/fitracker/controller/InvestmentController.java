package com.ohapps.fitracker.controller;

import com.ohapps.fitracker.dto.InvestmentDto;
import com.ohapps.fitracker.dto.InvestmentMetricDto;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.InvestmentUpdate;
import com.ohapps.fitracker.mapper.InvestmentMapper;
import com.ohapps.fitracker.security.CurrentUserService;
import com.ohapps.fitracker.service.InvestmentService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping(value = "/investment")
public class InvestmentController {

    private final CurrentUserService currentUserService;
    private final InvestmentService investmentService;
    private final InvestmentMapper investmentMapper;

    @Autowired
    public InvestmentController(CurrentUserService currentUserService, InvestmentService investmentService, InvestmentMapper investmentMapper) {
        this.currentUserService = currentUserService;
        this.investmentService = investmentService;
        this.investmentMapper = investmentMapper;
    }

    @ApiOperation(value = "get account types")
    @GetMapping(value = "/account-types")
    public ResponseEntity getAccountTypes(@RequestHeader(value = "Authorization") String authorization) {
        log.info("getTypes() - get account types");
        return ResponseEntity.ok(investmentService.accountTypes());
    }

    @ApiOperation(value = "get current user investments")
    @GetMapping()
    public ResponseEntity<List<Investment>> getUserInvestments(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request) {
        log.info("getUserInvestments() - get user investments requested: userId={}", currentUserService.getCurrentUser(request).getId());
        List<Investment> investments = investmentService.findByUserId(currentUserService.getCurrentUser(request).getId());
        log.info("getUserInvestments() - returned user investments: count={}", investments.size());
        return ResponseEntity.ok(investments);
    }

    @ApiOperation(value = "save current user investment")
    @PostMapping()
    public ResponseEntity saveInvestment(@RequestHeader(value = "Authorization") String authorization, @RequestBody InvestmentDto investment, HttpServletRequest request) {
        log.info("saveInvestment() - update user investment requested: userId={}", currentUserService.getCurrentUser(request).getId());
        InvestmentUpdate investmentUpdate = investmentService.saveForUserId(investmentMapper.convertDtoToEntity(investment), currentUserService.getCurrentUser(request).getId());
        if (investmentUpdate.isSuccess()) {
            log.info("saveInvestment() - user investment updated successfully: userId={}", currentUserService.getCurrentUser(request).getId());
            return ResponseEntity.ok(investmentMapper.convertEntityToDto(investmentUpdate.getInvestment()));
        } else {
            log.warn("saveInvestment() - investment does not belong to current user: userId={}", currentUserService.getCurrentUser(request).getId());
            return ResponseEntity.badRequest().body(investmentUpdate.getErrors());
        }
    }

    @ApiOperation(value = "delete current user investment")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteInvestment(@RequestHeader(value = "Authorization") String authorization, @PathVariable String id, HttpServletRequest request) {
        log.info("deleteInvestment() - delete user investment requested: userId={}", currentUserService.getCurrentUser(request).getId());
        InvestmentUpdate investmentUpdate = investmentService.deleteForUserId(id, currentUserService.getCurrentUser(request).getId());
        if (investmentUpdate.isSuccess()) {
            log.info("deleteInvestment() - user investment deleted successfully: userId={}", currentUserService.getCurrentUser(request).getId());
            return ResponseEntity.ok().build();
        } else {
            log.warn("deleteInvestment() - investment does not belong to current user: userId={}", currentUserService.getCurrentUser(request).getId());
            return ResponseEntity.badRequest().body(investmentUpdate.getErrors());
        }
    }

    @GetMapping("/{id}/metrics")
    public ResponseEntity getMetrics(@PathVariable String id, HttpServletRequest request) {
        List<InvestmentMetricDto> metrics = investmentService.getInvestmentMetrics(id, currentUserService.getCurrentUser(request).getId());
        return ResponseEntity.ok(metrics);
    }
}
