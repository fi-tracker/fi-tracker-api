package com.ohapps.fitracker.controller;

import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.entity.UserPortfolio;
import com.ohapps.fitracker.entity.portfolio.FreedomSummary;
import com.ohapps.fitracker.security.CurrentUserService;
import com.ohapps.fitracker.service.PortfolioService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@RequestMapping(value = "/portfolio")
public class PortfolioController {

    private final CurrentUserService currentUserService;
    private final PortfolioService portfolioService;

    @ApiOperation(value = "get current user profile")
    @GetMapping
    public ResponseEntity<UserPortfolio> getUserPortfolio(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request, @RequestParam(name = "includeRetirement", defaultValue = "true") String includeRetirement) {
        User user = currentUserService.getCurrentUser(request);
        log.info("getUserPortfolio() - get user portfolio requested: userId={}", user.getId());
        UserPortfolio userPortfolio = portfolioService.getUserPortfolio(user, "true".equalsIgnoreCase(includeRetirement));
        log.info("getUserPortfolio() - user portfolio successfully retrieved: userId={}", user.getId());
        return ResponseEntity.ok(userPortfolio);
    }

    @ApiOperation(value = "get current user freedom summary")
    @GetMapping("/freedom-tracker")
    public ResponseEntity<FreedomSummary> getFreedomSummary(@RequestHeader(value = "Authorization") String authorization, HttpServletRequest request) {
        User user = currentUserService.getCurrentUser(request);
        log.info("getFreedomSummary() - get user freedom summary requested: userId={}", user.getId());
        FreedomSummary freedomSummary = portfolioService.getUserFreedomSummary(user);
        log.info("getFreedomSummary() - user freedom summary successfully retrieved: userId={}", user.getId());
        return ResponseEntity.ok(freedomSummary);
    }
}
