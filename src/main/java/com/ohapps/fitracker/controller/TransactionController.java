package com.ohapps.fitracker.controller;

import com.ohapps.fitracker.dto.TransactionDto;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.TransactionUpdate;
import com.ohapps.fitracker.mapper.TransactionMapper;
import com.ohapps.fitracker.security.CurrentUserService;
import com.ohapps.fitracker.service.InvestmentService;
import com.ohapps.fitracker.service.TransactionService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping(value = "/transaction")
public class TransactionController {

    private final CurrentUserService currentUserService;
    private final InvestmentService investmentService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;

    @Autowired
    public TransactionController(CurrentUserService currentUserService,
                                 InvestmentService investmentService,
                                 TransactionService transactionService,
                                 TransactionMapper transactionMapper) {
        this.currentUserService = currentUserService;
        this.investmentService = investmentService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }

    @ApiOperation(value = "get transaction types")
    @GetMapping(value = "/types")
    public ResponseEntity getTypes(@RequestHeader(value = "Authorization") String authorization) {
        log.info("getTypes() - get transaction types");
        return ResponseEntity.ok(transactionService.transactionTypes());
    }

    @ApiOperation(value = "get transactions for investment")
    @GetMapping(value = "/{investmentId}")
    public ResponseEntity getTransactions(@RequestHeader(value = "Authorization") String authorization,
                                          @PathVariable String investmentId,
                                          @RequestParam(name = "pageSize", defaultValue = "20") int pageSize,
                                          HttpServletRequest request) {
        log.info("getInvestmentTransactions() - get transactions for investment: investmentId={}", investmentId);
        Investment investment = investmentService.findById(investmentId);
        if (investment != null && investmentService.isUsersInvestment(investment, currentUserService.getCurrentUser(request).getId())) {
            return ResponseEntity.ok(transactionService.findByInvestmentId(investment.getId(), pageSize));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @ApiOperation(value = "save transaction for investment")
    @PostMapping()
    public ResponseEntity saveTransaction(@RequestHeader(value = "Authorization") String authorization, @RequestBody TransactionDto transaction, HttpServletRequest request) {
        log.info("saveInvestmentTransactions() - save transactions for investment: investmentId={}", transaction.getInvestmentId());
        TransactionUpdate transactionUpdate = transactionService.saveForUserId(transactionMapper.convertDtoToEntity(transaction), currentUserService.getCurrentUser(request).getId());
        if (transactionUpdate.isSuccess()) {
            log.info("saveInvestmentTransactions() - transaction saved successfully: investmentId={}", transaction.getInvestmentId());
            return ResponseEntity.ok(transactionMapper.convertEntityToDto(transactionUpdate.getTransaction()));
        } else {
            log.warn("saveInvestmentTransactions() - transaction failed to save: investmentId={}", transaction.getInvestmentId());
            return ResponseEntity.badRequest().body(transactionUpdate.getErrors());
        }
    }

    @ApiOperation(value = "delete transaction for investment")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteTransaction(@RequestHeader(value = "Authorization") String authorization, @PathVariable String id, HttpServletRequest request) {
        log.info("deleteTransaction() - delete transaction for investment: id={}", id);
        TransactionUpdate transactionUpdate = transactionService.deleteByIdForUserId(id, currentUserService.getCurrentUser(request).getId());
        if (transactionUpdate.isSuccess()) {
            log.info("deleteTransaction() - transaction deleted successfully: id={}", id);
            return ResponseEntity.ok().build();
        } else {
            log.warn("deleteTransaction() - transaction failed to delete: id={}", id);
            return ResponseEntity.badRequest().body(transactionUpdate.getErrors());
        }
    }
}
