package com.ohapps.fitracker;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class FiTrackerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(FiTrackerApplication.class, args);
    }

    @Value("${app.config}")
    private String config;

    @Override
    public void run(String... args) throws Exception {
        log.info("app.config = {}", config);
    }
}
