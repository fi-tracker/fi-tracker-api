package com.ohapps.fitracker.util;

import com.ohapps.fitracker.constant.TransactionType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.Arrays;
import java.util.List;

public class InvestmentUtils {

    private InvestmentUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> getPassiveIncomeTypes() {
        return Arrays.asList(
                TransactionType.GAIN.getType(),
                TransactionType.GAIN_REINVESTED.getType(),
                TransactionType.LOSS.getType(),
                TransactionType.LOSS_WITHDRAWN.getType());
    }

    public static List<String> getAdditionalInvestmentTypes() {
        return Arrays.asList(
                TransactionType.GAIN_REINVESTED.getType(),
                TransactionType.PURCHASE_OF_INVESTMENT.getType(),
                TransactionType.LOSS_WITHDRAWN.getType());
    }

    public static List<String> getSaleOfInvestmentTypes() {
        return Arrays.asList(TransactionType.SALE_OF_INVESTMENT.getType());
    }

    public static LocalDateTime getBeginningOfCurrentMonth() {
        return LocalDateTime.of(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1), LocalTime.of(0, 0));
    }

    public static int getMonthsOfTransactions(LocalDateTime startDate, LocalDateTime endDate) {
        var period = Period.between(startDate.toLocalDate(), endDate.toLocalDate());
        return ((period.getYears() * 12) + period.getMonths());
    }
}
