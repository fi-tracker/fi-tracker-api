package com.ohapps.fitracker.entity;

import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@Document(collection = "users")
public class User {

    @Id
    private String id;
    @NonNull
    private String email;
    private String password;
    private BigDecimal monthlyExpenses;
    private BigDecimal targetIncome;
    private BigDecimal retirementWithdrawRate = BigDecimal.valueOf(4);

    @NonNull
    private int active;

    public User(String email, String password, int active) {
        this.email = email;
        this.password = password;
        this.active = active;
    }
}
