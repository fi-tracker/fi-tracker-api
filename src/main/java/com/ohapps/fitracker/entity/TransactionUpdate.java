package com.ohapps.fitracker.entity;

import lombok.Data;

@Data
public class TransactionUpdate extends BaseUpdate {

    private Transaction transaction;

}
