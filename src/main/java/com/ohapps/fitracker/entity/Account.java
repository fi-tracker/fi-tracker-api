package com.ohapps.fitracker.entity;

import com.ohapps.fitracker.dto.ModifyAccountDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "accounts")
public class Account {

    @Id
    private String id;
    @NotNull
    private String userId;
    private String description;
    private LocalDateTime inserted;
    private LocalDateTime updated;

    public static Account createFromModifyAccountDto(ModifyAccountDto modifyAccountDto, String userId) {
        return Account.builder()
                .description(modifyAccountDto.getDescription())
                .userId(userId)
                .inserted(LocalDateTime.now())
                .build();
    }

    public void updateFromModifyAccountDto(ModifyAccountDto modifyAccountDto) {
        description = modifyAccountDto.getDescription();
        updated = LocalDateTime.now();
    }
}
