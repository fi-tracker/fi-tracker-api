package com.ohapps.fitracker.entity;

import com.ohapps.fitracker.entity.portfolio.InvestmentSummary;
import com.ohapps.fitracker.entity.portfolio.MonthlySummary;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Data
public class UserPortfolio {

    private int investments;
    private BigDecimal portfolioValue = BigDecimal.ZERO;
    private BigDecimal portfolioDebt = BigDecimal.ZERO;
    private BigDecimal cashReserves = BigDecimal.ZERO;
    private BigDecimal monthsReserve = BigDecimal.ZERO;
    private BigDecimal targetIncome = BigDecimal.ZERO;
    private HashMap<String, BigDecimal> accountTypeBalances = new HashMap<>();
    private HashMap<String, BigDecimal> investmentTypeBalances = new HashMap<>();
    private List<MonthlySummary> passiveIncome = new ArrayList<>();
    private List<InvestmentSummary> investmentSummaries = new ArrayList<>();

    public BigDecimal getPortfolioEquity() {
        return portfolioValue.subtract(portfolioDebt);
    }
}
