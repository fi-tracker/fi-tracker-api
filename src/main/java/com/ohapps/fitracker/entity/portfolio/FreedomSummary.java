package com.ohapps.fitracker.entity.portfolio;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class FreedomSummary {
    private BigDecimal monthlyExpenses;
    private List<PreRetirementInvestment> preRetirementInvestments = new ArrayList<>();
    private List<PostRetirementInvestment> postRetirementInvestments = new ArrayList<>();
}
