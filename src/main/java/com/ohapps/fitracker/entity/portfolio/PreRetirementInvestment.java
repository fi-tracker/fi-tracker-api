package com.ohapps.fitracker.entity.portfolio;

import com.ohapps.fitracker.entity.Investment;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class PreRetirementInvestment {
    private Investment investment;
    private BigDecimal annualCashReturn;
    private BigDecimal averageMonthlyCashflow;
}
