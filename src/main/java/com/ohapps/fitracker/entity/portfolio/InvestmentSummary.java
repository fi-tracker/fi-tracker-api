package com.ohapps.fitracker.entity.portfolio;

import com.ohapps.fitracker.entity.Investment;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvestmentSummary {
    private Investment investment;
    private BigDecimal income;
    private BigDecimal appreciation;
    private BigDecimal roi;
    private BigDecimal capRate;
}
