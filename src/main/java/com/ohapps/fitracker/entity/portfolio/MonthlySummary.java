package com.ohapps.fitracker.entity.portfolio;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class MonthlySummary {
    private Integer year;
    private Integer month;
    private BigDecimal amount;
}
