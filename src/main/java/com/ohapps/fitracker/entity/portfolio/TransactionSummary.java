package com.ohapps.fitracker.entity.portfolio;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class TransactionSummary {
    private BigDecimal total;
}
