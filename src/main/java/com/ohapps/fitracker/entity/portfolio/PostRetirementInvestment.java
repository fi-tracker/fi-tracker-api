package com.ohapps.fitracker.entity.portfolio;

import com.ohapps.fitracker.entity.Investment;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class PostRetirementInvestment {
    private Investment investment;
    private BigDecimal annualWithdrawRate;
    private BigDecimal monthlyWithdrawAmount;
}
