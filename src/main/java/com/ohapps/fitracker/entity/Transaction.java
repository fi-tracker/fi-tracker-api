package com.ohapps.fitracker.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "transactions")
public class Transaction {

    @Id
    private String id;
    @NotNull
    private String investmentId;
    private LocalDate transactionDate;
    private String type;
    private String description;
    private BigDecimal amount;

    public Transaction(@NotNull String investmentId, LocalDate transactionDate, String type, String description, BigDecimal amount) {
        this.investmentId = investmentId;
        this.transactionDate = transactionDate;
        this.type = type;
        this.description = description;
        this.amount = amount;
    }
}
