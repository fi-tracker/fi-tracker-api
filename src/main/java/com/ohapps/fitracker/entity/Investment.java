package com.ohapps.fitracker.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "investments")
public class Investment {

    @Id
    private String id;
    @NotNull
    private String userId;
    private String description;
    private String type;
    private String accountType;
    private BigDecimal costBasis;
    private BigDecimal currentDebt;
    private BigDecimal currentValue;
    private LocalDateTime inserted;
    private LocalDateTime updated;
    private String accountId;

    public Investment(@NotNull String userId, String description, String type, String accountType, BigDecimal costBasis, BigDecimal currentDebt, BigDecimal currentValue) {
        this.userId = userId;
        this.description = description;
        this.type = type;
        this.accountType = accountType;
        this.costBasis = costBasis;
        this.currentDebt = currentDebt;
        this.currentValue = currentValue;
    }
}
