package com.ohapps.fitracker.entity;

import lombok.Data;

@Data
public class InvestmentUpdate extends BaseUpdate {

    private Investment investment;

}
