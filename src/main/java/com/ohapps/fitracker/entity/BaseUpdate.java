package com.ohapps.fitracker.entity;

import lombok.Data;

import java.util.List;

@Data
public abstract class BaseUpdate {

    private boolean success;
    private List<SystemError> errors;


}
