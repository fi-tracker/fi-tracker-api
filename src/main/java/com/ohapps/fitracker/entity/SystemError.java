package com.ohapps.fitracker.entity;

import com.ohapps.fitracker.constant.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SystemError {

    private String code;
    private String message;

    public SystemError(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }
}
