package com.ohapps.fitracker.dao;

import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.portfolio.MonthlySummary;
import com.ohapps.fitracker.entity.portfolio.TransactionSummary;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class PortfolioDao {

    private final MongoTemplate mongoTemplate;

    public List<MonthlySummary> getPassiveIncome(List<String> investmentIds, LocalDateTime startDate, LocalDateTime endDate, List<String> transactionTypes) {

        MatchOperation matchStage = Aggregation.match(new Criteria("investmentId").in(investmentIds)
                .and("type").in(transactionTypes)
                .and("transactionDate").gte(startDate).lt(endDate)
        );

        ProjectionOperation projectStage = Aggregation.project("amount")
                .andExpression("year(transactionDate)").as("year")
                .andExpression("month(transactionDate)").as("month");

        GroupOperation groupStage = Aggregation.group("year", "month")
                .sum(ConvertOperators.valueOf("amount").convertToDouble())
                .as("amount");

        SortOperation sortStage = Aggregation.sort(new Sort(Sort.Direction.ASC, "_id"));

        Aggregation aggregation = Aggregation.newAggregation(matchStage, projectStage, groupStage, sortStage);
        AggregationResults<MonthlySummary> results = mongoTemplate.aggregate(aggregation, "transactions", MonthlySummary.class);

        return results.getMappedResults();
    }

    public BigDecimal getTransactionTotal(String investmentId, LocalDateTime startDate, LocalDateTime endDate, List<String> transactionTypes) {

        MatchOperation matchStage = Aggregation.match(new Criteria("investmentId").in(investmentId)
                .and("type").in(transactionTypes)
                .and("transactionDate").gte(startDate).lt(endDate)
        );

        GroupOperation groupStage = Aggregation.group("investmentId")
                .sum(ConvertOperators.valueOf("amount").convertToDouble())
                .as("total");

        ProjectionOperation projectStage = Aggregation.project("total").andExclude("_id");

        Aggregation aggregation = Aggregation.newAggregation(matchStage, groupStage, projectStage);
        AggregationResults<TransactionSummary> results = mongoTemplate.aggregate(aggregation, "transactions", TransactionSummary.class);

        if (!CollectionUtils.isEmpty(results.getMappedResults())) {
            return results.getMappedResults().get(0).getTotal();
        }
        return BigDecimal.ZERO;
    }

    public Optional<Transaction> getOldestTransaction(String investmentId, LocalDateTime startDate, LocalDateTime endDate, List<String> transactionTypes) {

        Query query = new Query();
        query.addCriteria(Criteria.where("investmentId").is(investmentId))
                .addCriteria(Criteria.where("type").in(transactionTypes))
                .addCriteria(Criteria.where("transactionDate").gte(startDate).lt(endDate))
                .with(new Sort(Sort.Direction.ASC, "transactionDate"));

        Transaction transaction = mongoTemplate.findOne(query, Transaction.class);

        if (transaction != null) {
            return Optional.of(transaction);
        }

        return Optional.empty();
    }

    public Optional<LocalDate> getFirstTransactionDate(String investmentId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("investmentId").is(investmentId)).with(new Sort(Sort.Direction.ASC, "transactionDate"));

        Transaction transaction = mongoTemplate.findOne(query, Transaction.class);
        return transaction != null ? Optional.of(transaction.getTransactionDate()) : Optional.empty();
    }
}
