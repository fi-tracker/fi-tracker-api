package com.ohapps.fitracker.service;

import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.entity.UserPortfolio;
import com.ohapps.fitracker.entity.portfolio.FreedomSummary;

public interface PortfolioService {

    UserPortfolio getUserPortfolio(User user, Boolean includeRetirementAccount);
    FreedomSummary getUserFreedomSummary(User user);

}
