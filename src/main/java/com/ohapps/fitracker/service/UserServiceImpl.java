package com.ohapps.fitracker.service;

import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.exception.InvalidUserException;
import com.ohapps.fitracker.repository.UserRepository;
import com.ohapps.fitracker.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserValidator userValidator;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findById(String id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        throw new InvalidUserException("invalid user id: + " + id);
    }

    public User save(User user) {
        userValidator.isValid(user);
        return userRepository.save(user);
    }

    public User register(String email, String password) {
        User user = new User(email, password, 1);
        save(user);
        return user;
    }

    public void delete(User user) {
        userRepository.delete(user);
    }
}
