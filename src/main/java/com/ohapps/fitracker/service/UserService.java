package com.ohapps.fitracker.service;

import com.ohapps.fitracker.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(String id);

    User findByEmail(String email);

    User save(User user);

    User register(String email, String password);

    void delete(User user);

}
