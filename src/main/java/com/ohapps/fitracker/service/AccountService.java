package com.ohapps.fitracker.service;

import com.ohapps.fitracker.dto.ModifyAccountDto;
import com.ohapps.fitracker.entity.Account;
import com.ohapps.fitracker.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Service
@RequiredArgsConstructor
@Validated
public class AccountService {

    private final AccountRepository accountRepository;

    public List<Account> findByUserId(String userId) {
        return accountRepository.findByUserIdOrderByDescription(userId);
    }

    public Account createAccount(@Valid ModifyAccountDto modifyAccountDto, @NotBlank String userId) {
        Account account = Account.createFromModifyAccountDto(modifyAccountDto, userId);
        return accountRepository.save(account);
    }

    public Account updateAccount(@Valid ModifyAccountDto modifyAccountDto, @NotBlank String id, @NotBlank String userId) {
        Account account = findByIdForUserId(id, userId);
        account.updateFromModifyAccountDto(modifyAccountDto);
        return accountRepository.save(account);
    }

    public void deleteAccount(@NotBlank String id, @NotBlank String userId) {
        Account account = findByIdForUserId(id, userId);
        accountRepository.delete(account);
    }

    public Account findByIdForUserId(String id, String userId) {
        return accountRepository.findByIdAndUserId(id, userId)
                .orElseThrow(() -> new AccessDeniedException(String.format("access denied for user id %s when trying to find account id %s", userId, id)));
    }
}
