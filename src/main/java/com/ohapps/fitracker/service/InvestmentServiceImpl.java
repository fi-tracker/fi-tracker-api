package com.ohapps.fitracker.service;

import com.ohapps.fitracker.constant.AccountType;
import com.ohapps.fitracker.constant.ErrorCode;
import com.ohapps.fitracker.constant.MetricType;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.dao.PortfolioDao;
import com.ohapps.fitracker.dto.InvestmentMetricDto;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.InvestmentUpdate;
import com.ohapps.fitracker.entity.SystemError;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.portfolio.MonthlySummary;
import com.ohapps.fitracker.repository.InvestmentRepository;
import com.ohapps.fitracker.util.InvestmentUtils;
import com.ohapps.fitracker.validation.InvestmentValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class InvestmentServiceImpl implements InvestmentService {

    private final InvestmentRepository investmentRepository;
    private final InvestmentValidator investmentValidator;
    private final PortfolioDao portfolioDao;

    @Override
    public List<String> accountTypes() {
        return AccountType.getTypes();
    }

    @Override
    public List<Investment> findByUserId(String userId) {
        return investmentRepository.findByUserId(userId);
    }

    @Override
    public Investment findById(String id) {

        if (id == null) {
            return null;
        }

        return this.investmentRepository.findById(id).orElse(null);
    }

    @Override
    public Investment save(Investment investment) {

        if (investment.getId() == null) {
            investment.setInserted(LocalDateTime.now());
        } else {
            investment.setUpdated(LocalDateTime.now());
        }
        return investmentRepository.save(investment);
    }

    @Override
    public InvestmentUpdate saveForUserId(Investment investment, String userId) {
        InvestmentUpdate investmentUpdate = new InvestmentUpdate();

        if (!isUsersInvestment(investment, userId)) {
            investmentUpdate.setErrors(Collections.singletonList(new SystemError(ErrorCode.ACCESS_DENIED)));
            investmentUpdate.setSuccess(false);
            return investmentUpdate;
        }

        List<SystemError> errors = investmentValidator.isValidInvestment(investment);

        if (CollectionUtils.isEmpty(errors)) {
            investmentUpdate.setInvestment(save(investment));
            investmentUpdate.setSuccess(true);
        } else {
            investmentUpdate.setErrors(errors);
            investmentUpdate.setSuccess(false);
        }

        return investmentUpdate;
    }

    @Override
    public void delete(Investment investment) {
        investmentRepository.delete(investment);
    }

    @Override
    public InvestmentUpdate deleteForUserId(String investmentId, String userId) {
        InvestmentUpdate investmentUpdate = new InvestmentUpdate();

        Investment investment = findById(investmentId);

        if (investment == null) {
            investmentUpdate.setErrors(Collections.singletonList(new SystemError(ErrorCode.INVALID_INVESTMENT_ID)));
            investmentUpdate.setSuccess(false);
            return investmentUpdate;
        }

        if (!isUsersInvestment(investment, userId)) {
            investmentUpdate.setErrors(Collections.singletonList(new SystemError(ErrorCode.ACCESS_DENIED)));
            investmentUpdate.setSuccess(false);
        } else {
            delete(investment);
            investmentUpdate.setSuccess(true);
        }
        return investmentUpdate;
    }

    @Override
    public boolean isUsersInvestment(Investment investment, String userId) {
        if (investment == null) {
            return false;
        }
        if (investment.getId() != null) {
            Investment existingInvestment = this.findById(investment.getId());
            // overwrite userId from request with database record to avoid spoofing user id
            if (existingInvestment != null) {
                investment.setUserId(existingInvestment.getUserId());
            }
        } else {
            investment.setUserId(userId);
        }
        return (userId != null && investment != null && userId.equals(investment.getUserId()));
    }

    @Override
    public void transactionSaved(Transaction originalTransaction, Transaction updatedTransaction) {

        if (updatedTransaction == null) {
            log.warn("transactionSaved() - updatedTransaction is null when processing saved transaction auto update");
            return;
        }

        Investment investment = findById(updatedTransaction.getInvestmentId());

        if (investment == null) {
            log.error("transactionSaved() - investment is null when processing saved transaction auto update");
            return;
        }

        boolean reversed = transactionReversed(originalTransaction, investment);
        boolean applied = transactionApplied(updatedTransaction, investment);

        if (reversed || applied) {
            save(investment);
        }
    }

    @Override
    public void transactionDeleted(Transaction transaction) {

        if (transaction == null) {
            log.warn("transactionDeleted() - transaction is null when processing deleted transaction auto update");
            return;
        }

        Investment investment = findById(transaction.getInvestmentId());

        if (investment == null) {
            log.error("transactionDeleted() - investment is null when processing deleted transaction auto update");
            return;
        }

        if (transactionReversed(transaction, investment)) {
            save(investment);
        }
    }

    @Override
    public List<InvestmentMetricDto> getInvestmentMetrics(String investmentId, String userId) {

        Investment investment = findByIdForUser(investmentId, userId);
        List<InvestmentMetricDto> metrics = new ArrayList<>();

        // calculate dates
        LocalDate firstTransactionDate = portfolioDao.getFirstTransactionDate(investment.getId()).orElse(null);

        if (firstTransactionDate == null) {
            return metrics;
        }

        LocalDateTime endDate = LocalDateTime.of(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1), LocalTime.of(0, 0));
        LocalDateTime startDate = endDate.minusYears(1);

        if (startDate.isBefore(firstTransactionDate.atStartOfDay())) {
            startDate = firstTransactionDate.atStartOfDay();
        }

        var monthsOfTransactions = InvestmentUtils.getMonthsOfTransactions(startDate, endDate);

        // get cashflow
        List<MonthlySummary> monthlySummaries = portfolioDao.getPassiveIncome(List.of(investment.getId()), startDate, endDate, InvestmentUtils.getPassiveIncomeTypes());

        BigDecimal yearlyCashflow = monthlySummaries.stream()
                .map(MonthlySummary::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal monthlyCashflow = (monthlySummaries.isEmpty() || monthsOfTransactions == 0) ? BigDecimal.ZERO : yearlyCashflow.divide(BigDecimal.valueOf(monthsOfTransactions), RoundingMode.HALF_UP);

        List<MonthlySummary> allMonthlySummaries = portfolioDao.getPassiveIncome(List.of(investment.getId()), firstTransactionDate.atStartOfDay(), endDate, InvestmentUtils.getPassiveIncomeTypes());

        BigDecimal lifetimeCashflow = allMonthlySummaries.stream()
                .map(MonthlySummary::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        metrics.add(InvestmentMetricDto.builder()
                .description("Cashflow")
                .monthlyAverage(monthlyCashflow)
                .yearlyTotal(yearlyCashflow)
                .lifetimeTotal(lifetimeCashflow)
                .metricType(MetricType.DOLLAR)
                .build());

        // get appreciation
        BigDecimal yearlyAppreciate = calculateAppreciation(investment, startDate, endDate);
        BigDecimal monthlyAppreciation = BigDecimal.ZERO.equals(yearlyAppreciate) || monthsOfTransactions == 0 ? BigDecimal.ZERO : yearlyAppreciate.divide(BigDecimal.valueOf(monthsOfTransactions), RoundingMode.HALF_UP);
        BigDecimal lifetimeAppreciation = calculateAppreciation(investment, firstTransactionDate.atStartOfDay(), endDate);

        metrics.add(InvestmentMetricDto.builder()
                .description("Appreciation")
                .monthlyAverage(monthlyAppreciation)
                .yearlyTotal(yearlyAppreciate)
                .lifetimeTotal(lifetimeAppreciation)
                .metricType(MetricType.DOLLAR)
                .build());

        // calculate ROI
        BigDecimal monthlyRoi = calculateRoi(investment, monthlyCashflow, monthlyAppreciation);
        BigDecimal yearlyRoi = calculateRoi(investment, yearlyCashflow, yearlyAppreciate);
        BigDecimal lifetimeRoi = calculateRoi(investment, lifetimeCashflow, lifetimeAppreciation);

        metrics.add(InvestmentMetricDto.builder()
                .description("ROI")
                .monthlyAverage(monthlyRoi)
                .yearlyTotal(yearlyRoi)
                .lifetimeTotal(lifetimeRoi)
                .metricType(MetricType.PERCENT)
                .build());

        return metrics;
    }

    @Override
    public BigDecimal calculateAppreciation(Investment investment, LocalDateTime startDate, LocalDateTime endDate) {

        Optional<Transaction> oldestValue = portfolioDao.getOldestTransaction(investment.getId(), startDate, endDate, Collections.singletonList(TransactionType.CHANGE_IN_VALUE.getType()));

        if (oldestValue.isPresent()) {

            LocalDateTime newStartDate = LocalDateTime.of(oldestValue.get().getTransactionDate(), LocalTime.of(0, 0));

            // sum all additional purchases minus loss since oldest value
            BigDecimal additionalInvestment = portfolioDao.getTransactionTotal(investment.getId(), newStartDate, endDate, InvestmentUtils.getAdditionalInvestmentTypes());

            // sum all sales since oldest value
            BigDecimal saleOfInvestment = portfolioDao.getTransactionTotal(investment.getId(), newStartDate, endDate, InvestmentUtils.getSaleOfInvestmentTypes());

            return investment.getCurrentValue().subtract(oldestValue.get().getAmount()).subtract(additionalInvestment.subtract(saleOfInvestment));
        }

        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal calculateRoi(Investment investment, BigDecimal income, BigDecimal appreciation) {
        // income + appreciation / cost basis
        if (BigDecimal.ZERO.compareTo(investment.getCostBasis()) < 0 && income != null && appreciation != null) {
            return income.add(appreciation).divide(investment.getCostBasis(), 4, RoundingMode.HALF_UP);
        }
        return BigDecimal.ZERO;
    }

    @Override
    public void clearAccountIdForUserId(String accountId, String userId) {
        List<Investment> investments = investmentRepository.findByUserIdAndAccountId(userId, accountId);
        investments.stream()
                .peek(investment -> investment.setAccountId(null))
                .forEach(investmentRepository::save);
    }

    private Investment findByIdForUser(String investmentId, String userId) {
        Investment investment = findById(investmentId);

        if (!investment.getUserId().equals(userId)) {
            throw new AccessDeniedException("invalid investment id");
        }

        return investment;
    }

    private boolean transactionReversed(Transaction transaction, Investment investment) {

        if (transaction != null && transaction.getType() != null) {
            if (TransactionType.GAIN_REINVESTED.getType().equals(transaction.getType()) && investment.getCurrentValue() != null) {
                investment.setCurrentValue(investment.getCurrentValue().subtract(transaction.getAmount()));
                return true;
            }
            if (TransactionType.LOSS_WITHDRAWN.getType().equals(transaction.getType()) && investment.getCurrentValue() != null) {
                investment.setCurrentValue(investment.getCurrentValue().subtract(transaction.getAmount()));
                return true;
            }
            if (TransactionType.PURCHASE_OF_INVESTMENT.getType().equals(transaction.getType()) && investment.getCurrentValue() != null && investment.getCostBasis() != null) {
                investment.setCurrentValue(investment.getCurrentValue().subtract(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().subtract(transaction.getAmount()));
                return true;
            }
            if (TransactionType.SALE_OF_INVESTMENT.getType().equals(transaction.getType()) && investment.getCurrentValue() != null && investment.getCostBasis() != null) {
                investment.setCurrentValue(investment.getCurrentValue().add(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().add(transaction.getAmount()));
                return true;
            }
            if (TransactionType.DEBT_REPAYMENT.getType().equals(transaction.getType()) && investment.getCurrentDebt() != null && investment.getCostBasis() != null) {
                investment.setCurrentDebt(investment.getCurrentDebt().add(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().subtract(transaction.getAmount()));
                return true;
            }
        }
        return false;
    }

    private boolean transactionApplied(Transaction transaction, Investment investment) {

        if (transaction != null && transaction.getType() != null) {
            if (TransactionType.GAIN_REINVESTED.getType().equals(transaction.getType()) && investment.getCurrentValue() != null) {
                investment.setCurrentValue(investment.getCurrentValue().add(transaction.getAmount()));
                return true;
            }
            if (TransactionType.LOSS_WITHDRAWN.getType().equals(transaction.getType()) && investment.getCurrentValue() != null) {
                investment.setCurrentValue(investment.getCurrentValue().add(transaction.getAmount()));
                return true;
            }
            if (TransactionType.PURCHASE_OF_INVESTMENT.getType().equals(transaction.getType()) && investment.getCurrentValue() != null && investment.getCostBasis() != null) {
                investment.setCurrentValue(investment.getCurrentValue().add(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().add(transaction.getAmount()));
                return true;
            }
            if (TransactionType.SALE_OF_INVESTMENT.getType().equals(transaction.getType()) && investment.getCurrentValue() != null && investment.getCostBasis() != null) {
                investment.setCurrentValue(investment.getCurrentValue().subtract(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().subtract(transaction.getAmount()));
                return true;
            }
            if (TransactionType.CHANGE_IN_VALUE.getType().equals(transaction.getType())) {
                investment.setCurrentValue(transaction.getAmount());
                return true;
            }
            if (TransactionType.DEBT_REPAYMENT.getType().equals(transaction.getType()) && investment.getCurrentDebt() != null && investment.getCostBasis() != null) {
                investment.setCurrentDebt(investment.getCurrentDebt().subtract(transaction.getAmount()));
                investment.setCostBasis(investment.getCostBasis().add(transaction.getAmount()));
                return true;
            }
        }
        return false;
    }
}
