package com.ohapps.fitracker.service;

import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.TransactionUpdate;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TransactionService {

    List<String> transactionTypes();
    Page<Transaction> findByInvestmentId(String investmentId, int pageSize);
    Transaction findById(String id);
    Transaction save(Transaction transaction, boolean autoUpdateInvestment);
    TransactionUpdate saveForUserId(Transaction transaction, String userId);
    void delete(Transaction transaction, boolean autoUpdateInvestment);
    TransactionUpdate deleteByIdForUserId(String id, String userId);

}
