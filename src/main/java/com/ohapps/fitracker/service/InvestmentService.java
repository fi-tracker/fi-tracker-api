package com.ohapps.fitracker.service;

import com.ohapps.fitracker.dto.InvestmentMetricDto;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.InvestmentUpdate;
import com.ohapps.fitracker.entity.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface InvestmentService {

    List<String> accountTypes();
    List<Investment> findByUserId(String userId);
    Investment findById(String id);
    Investment save(Investment investment);
    InvestmentUpdate saveForUserId(Investment investment, String userId);
    void delete(Investment investment);
    InvestmentUpdate deleteForUserId(String investmentId, String userId);
    boolean isUsersInvestment(Investment investment, String userId);
    void transactionSaved(Transaction originalTransaction, Transaction updatedTransaction);
    void transactionDeleted(Transaction transaction);
    List<InvestmentMetricDto> getInvestmentMetrics(String investmentId, String userId);
    BigDecimal calculateAppreciation(Investment investment, LocalDateTime startDate, LocalDateTime endDate);
    BigDecimal calculateRoi(Investment investment, BigDecimal income, BigDecimal appreciation);
    void clearAccountIdForUserId(String accountId, String userId);
}
