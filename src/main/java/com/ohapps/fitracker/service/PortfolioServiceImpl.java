package com.ohapps.fitracker.service;

import com.ohapps.fitracker.constant.AccountType;
import com.ohapps.fitracker.dao.PortfolioDao;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.entity.UserPortfolio;
import com.ohapps.fitracker.entity.portfolio.*;
import com.ohapps.fitracker.util.InvestmentUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PortfolioServiceImpl implements PortfolioService {

    private final InvestmentService investmentService;
    private final PortfolioDao portfolioDao;

    @Override
    public UserPortfolio getUserPortfolio(User user, Boolean includeRetirementAccount) {
        UserPortfolio userPortfolio = new UserPortfolio();

        if (user == null) {
            return userPortfolio;
        }

        userPortfolio.setTargetIncome(user.getTargetIncome());

        LocalDateTime endDate = InvestmentUtils.getBeginningOfCurrentMonth();
        LocalDateTime startDate = endDate.minusYears(1);

        List<Investment> investments = investmentService.findByUserId(user.getId());

        if (!CollectionUtils.isEmpty(investments)) {
            userPortfolio.setInvestments(investments.size());

            HashMap<String, BigDecimal> accountBalances = new HashMap<>();
            HashMap<String, BigDecimal> investmentTypeBalances = new HashMap<>();

            for (Investment investment : investments) {
                addInvestmentToPortfolio(userPortfolio, accountBalances, investmentTypeBalances, investment);
                userPortfolio.getInvestmentSummaries().add(calculateInvestmentSummary(investment, startDate, endDate));
            }

            userPortfolio.setAccountTypeBalances(accountBalances);
            userPortfolio.setInvestmentTypeBalances(investmentTypeBalances);

            if (userPortfolio.getCashReserves().compareTo(BigDecimal.ZERO) > 0 && user.getMonthlyExpenses() != null && user.getMonthlyExpenses().compareTo(BigDecimal.ZERO) > 0) {
                userPortfolio.setMonthsReserve(userPortfolio.getCashReserves().divide(user.getMonthlyExpenses(), 2, RoundingMode.HALF_UP));
            }

            userPortfolio.setPassiveIncome(loadPassiveIncome(investments.stream()
                    .filter(investment -> !"retirement".equalsIgnoreCase(investment.getAccountType()) || includeRetirementAccount)
                    .map(Investment::getId).collect(Collectors.toList()), startDate, endDate));
        }

        return userPortfolio;
    }

    @Override
    public FreedomSummary getUserFreedomSummary(User user) {
        var freedomSummary = new FreedomSummary();
        freedomSummary.setMonthlyExpenses(user.getMonthlyExpenses());
        var endDate = InvestmentUtils.getBeginningOfCurrentMonth();
        var startDate = endDate.minusYears(1);
        var withdrawRate = user.getRetirementWithdrawRate().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);

        List<Investment> investments = investmentService.findByUserId(user.getId());

        // process pre-retirement accounts
        investments.stream()
                .filter(investment -> !AccountType.RETIREMENT.getType().equals(investment.getAccountType()))
                .forEach(investment -> {
                    // calculate average monthly cashflow for past year
                    var firstTransactionDate = portfolioDao.getFirstTransactionDate(investment.getId());
                    var investmentStartDate = (firstTransactionDate.isPresent() && startDate.isBefore(firstTransactionDate.get().atStartOfDay())) ? firstTransactionDate.get().atStartOfDay() : startDate;
                    var monthsOfTransactions = InvestmentUtils.getMonthsOfTransactions(investmentStartDate, endDate);
                    var totalIncome = portfolioDao.getTransactionTotal(investment.getId(), investmentStartDate, endDate, InvestmentUtils.getPassiveIncomeTypes());
                    var averageMonthlyCashflow = (BigDecimal.ZERO.compareTo(totalIncome) < 0 && monthsOfTransactions > 0) ? totalIncome.divide(BigDecimal.valueOf(monthsOfTransactions), RoundingMode.HALF_UP) : BigDecimal.ZERO;
                    var annualCashReturn = totalIncome.divide(investment.getCostBasis(), MathContext.DECIMAL128);
                    freedomSummary.getPreRetirementInvestments().add(PreRetirementInvestment.builder()
                            .investment(investment)
                            .averageMonthlyCashflow(averageMonthlyCashflow)
                            .annualCashReturn(annualCashReturn)
                            .build());
                });

        // process post-retirement accounts
        investments.stream()
                .filter(investment -> AccountType.RETIREMENT.getType().equals(investment.getAccountType()))
                .forEach(investment -> {
                    var monthlyWithdrawAmount = investment.getCurrentValue().multiply(withdrawRate).divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP);
                    freedomSummary.getPostRetirementInvestments().add(PostRetirementInvestment.builder()
                            .investment(investment)
                            .annualWithdrawRate(withdrawRate)
                            .monthlyWithdrawAmount(monthlyWithdrawAmount)
                            .build());
                });

        return freedomSummary;
    }

    private void addInvestmentToPortfolio(UserPortfolio userPortfolio, HashMap<String, BigDecimal> accountBalances, HashMap<String, BigDecimal> investmentTypeBalances, Investment investment) {
        if (investment.getCurrentValue() != null) {
            userPortfolio.setPortfolioValue(userPortfolio.getPortfolioValue().add(investment.getCurrentValue()));
        }

        if (investment.getCurrentDebt() != null) {
            userPortfolio.setPortfolioDebt(userPortfolio.getPortfolioDebt().add(investment.getCurrentDebt()));
        }

        if (investment.getAccountType() != null && AccountType.CASHRESERVE.getType().equalsIgnoreCase(investment.getAccountType())) {
            userPortfolio.setCashReserves(userPortfolio.getCashReserves().add(investment.getCurrentValue()));
        }

        if (investment.getAccountType() != null) {
            if (accountBalances.containsKey(investment.getAccountType())) {
                accountBalances.put(investment.getAccountType(), accountBalances.get(investment.getAccountType()).add(investment.getCurrentValue()));
            } else {
                accountBalances.put(investment.getAccountType(), investment.getCurrentValue());
            }
        }

        if (investment.getType() != null) {
            if (investmentTypeBalances.containsKey(investment.getType())) {
                investmentTypeBalances.put(investment.getType(), investmentTypeBalances.get(investment.getType()).add(investment.getCurrentValue()));
            } else {
                investmentTypeBalances.put(investment.getType(), investment.getCurrentValue());
            }
        }
    }

    private List<MonthlySummary> loadPassiveIncome(List<String> investmentIds, LocalDateTime startDate, LocalDateTime endDate) {
        return portfolioDao.getPassiveIncome(investmentIds, startDate, endDate, InvestmentUtils.getPassiveIncomeTypes());
    }

    private InvestmentSummary calculateInvestmentSummary(Investment investment, LocalDateTime startDate, LocalDateTime endDate) {
        InvestmentSummary investmentSummary = new InvestmentSummary();
        investmentSummary.setInvestment(investment);
        investmentSummary.setIncome(portfolioDao.getTransactionTotal(investment.getId(), startDate, endDate, InvestmentUtils.getPassiveIncomeTypes()));

        investmentSummary.setAppreciation(investmentService.calculateAppreciation(investment, startDate, endDate));

        // income + appreciation / cost basis
        if (BigDecimal.ZERO.compareTo(investment.getCostBasis()) < 0) {
            investmentSummary.setRoi(investmentSummary.getIncome().add(investmentSummary.getAppreciation()).divide(investment.getCostBasis(), 4, RoundingMode.HALF_UP));
        } else {
            investmentSummary.setRoi(BigDecimal.ZERO);
        }

        // income / current value
        if (BigDecimal.ZERO.compareTo(investment.getCurrentValue()) < 0) {
            investmentSummary.setCapRate(investmentSummary.getIncome().divide(investment.getCurrentValue(), 4, RoundingMode.HALF_UP));
        } else {
            investmentSummary.setCapRate(BigDecimal.ZERO);
        }

        return investmentSummary;
    }
}
