package com.ohapps.fitracker.service;

import com.ohapps.fitracker.constant.ErrorCode;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.SystemError;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.entity.TransactionUpdate;
import com.ohapps.fitracker.repository.TransactionRepository;
import com.ohapps.fitracker.validation.TransactionValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final InvestmentService investmentService;
    private final TransactionValidator transactionValidator;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  InvestmentService investmentService,
                                  TransactionValidator transactionValidator) {
        this.transactionRepository = transactionRepository;
        this.investmentService = investmentService;
        this.transactionValidator = transactionValidator;
    }

    @Override
    public List<String> transactionTypes() {
        return TransactionType.getTypes();
    }

    @Override
    public Page<Transaction> findByInvestmentId(String investmentId, int pageSize) {
        return transactionRepository.findByInvestmentIdOrderByTransactionDateDesc(investmentId, PageRequest.of(0, pageSize));
    }

    @Override
    public Transaction save(Transaction transaction, boolean autoUpdateInvestment) {

        Transaction originalTransaction = this.findById(transaction.getId());

        formatTransaction(transaction);

        transactionRepository.save(transaction);

        if (autoUpdateInvestment) {
            investmentService.transactionSaved(originalTransaction, transaction);
        }

        return transaction;
    }

    @Override
    public TransactionUpdate saveForUserId(Transaction transaction, String userId) {
        TransactionUpdate transactionUpdate = new TransactionUpdate();

        List<SystemError> errors = transactionValidator.isValidTransaction(transaction, userId);

        if (CollectionUtils.isEmpty(errors)) {
            transactionUpdate.setTransaction(save(transaction, true));
            transactionUpdate.setSuccess(true);
        } else {
            transactionUpdate.setErrors(errors);
            transactionUpdate.setSuccess(false);
        }

        return transactionUpdate;
    }

    @Override
    public void delete(Transaction transaction, boolean autoUpdateInvestment) {
        transactionRepository.delete(transaction);
        if (autoUpdateInvestment) {
            investmentService.transactionDeleted(transaction);
        }
    }

    @Override
    public TransactionUpdate deleteByIdForUserId(String id, String userId) {
        TransactionUpdate transactionUpdate = new TransactionUpdate();

        Transaction transaction = findById(id);

        List<SystemError> errors = transactionValidator.isValidTransactionForDelete(transaction, userId);

        if (CollectionUtils.isEmpty(errors)) {
            delete(transaction, true);

            // verify transaction was deleted
            if (findById(id) != null) {
                transactionUpdate.setSuccess(false);
                transactionUpdate.setErrors(Collections.singletonList(new SystemError(ErrorCode.SYSTEM_TRANSACTION_DELETE)));
            } else {
                transactionUpdate.setSuccess(true);
            }
        } else {
            transactionUpdate.setSuccess(false);
            transactionUpdate.setErrors(errors);
        }

        return transactionUpdate;
    }

    @Override
    public Transaction findById(String id) {

        if (id == null) {
            return null;
        }

        Optional<Transaction> optional = this.transactionRepository.findById(id);

        if (optional.isPresent()) {
            return optional.get();
        }

        return null;
    }

    private void formatTransaction(Transaction transaction) {

        // format loss as negative number
        if ((TransactionType.LOSS.getType().equals(transaction.getType()) ||
                TransactionType.LOSS_WITHDRAWN.getType().equals(transaction.getType())) &&
                BigDecimal.ZERO.compareTo(transaction.getAmount()) < 0
        ) {
            transaction.setAmount(transaction.getAmount().multiply(BigDecimal.valueOf(-1)));
        }
    }
}
