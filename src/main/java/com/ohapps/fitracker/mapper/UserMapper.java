package com.ohapps.fitracker.mapper;

import com.ohapps.fitracker.dto.ModifyUserDto;
import com.ohapps.fitracker.dto.UserDto;
import com.ohapps.fitracker.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserMapper {

    public UserDto convertEntityToDto(User user) {
        UserDto userDto = new UserDto();
        if (user != null) {
            userDto.setId(user.getId());
            userDto.setEmail(user.getEmail());
            userDto.setMonthlyExpenses(user.getMonthlyExpenses());
            userDto.setTargetIncome(user.getTargetIncome());
            userDto.setRetirementWithdrawRate(user.getRetirementWithdrawRate());
        }
        return userDto;
    }

    public void updateEntityFromDto(User user, ModifyUserDto modifyUserDto){
        user.setEmail(modifyUserDto.getEmail());
        if (!StringUtils.isEmpty(modifyUserDto.getPassword())) {
            user.setPassword(modifyUserDto.getPassword());
        }
        user.setMonthlyExpenses(modifyUserDto.getMonthlyExpenses());
        user.setTargetIncome(modifyUserDto.getTargetIncome());
        user.setRetirementWithdrawRate(modifyUserDto.getRetirementWithdrawRate());
    }
}
