package com.ohapps.fitracker.mapper;

import com.ohapps.fitracker.dto.InvestmentDto;
import com.ohapps.fitracker.entity.Investment;
import org.springframework.stereotype.Component;

@Component
public class InvestmentMapper {

    public Investment convertDtoToEntity(InvestmentDto investmentDto) {
        Investment investment = new Investment();
        investment.setId(investmentDto.getId());
        investment.setUserId(investmentDto.getUserId());
        investment.setDescription(investmentDto.getDescription());
        investment.setType(investmentDto.getType());
        investment.setAccountType(investmentDto.getAccountType());
        investment.setCostBasis(investmentDto.getCostBasis());
        investment.setCurrentDebt(investmentDto.getCurrentDebt());
        investment.setCurrentValue(investmentDto.getCurrentValue());
        investment.setAccountId(investmentDto.getAccountId());
        return investment;
    }

    public InvestmentDto convertEntityToDto(Investment investment) {
        if (investment == null) {
            return null;
        }
        return new InvestmentDto(investment.getId(),
                investment.getUserId(),
                investment.getDescription(),
                investment.getType(),
                investment.getAccountType(),
                investment.getCostBasis(),
                investment.getCurrentDebt(),
                investment.getCurrentValue(),
                investment.getAccountId());
    }
}
