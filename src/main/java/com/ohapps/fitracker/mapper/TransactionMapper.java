package com.ohapps.fitracker.mapper;

import com.ohapps.fitracker.dto.TransactionDto;
import com.ohapps.fitracker.entity.Transaction;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {

    public Transaction convertDtoToEntity(TransactionDto transactionDto) {
        if (transactionDto == null) {
            return null;
        }

        return new Transaction(
                transactionDto.getId(),
                transactionDto.getInvestmentId(),
                transactionDto.getTransactionDate(),
                transactionDto.getType(),
                transactionDto.getDescription(),
                transactionDto.getAmount());
    }

    public TransactionDto convertEntityToDto(Transaction transaction) {
        if (transaction == null) {
            return null;
        }

        return new TransactionDto(transaction.getId(),
                transaction.getInvestmentId(),
                transaction.getTransactionDate(),
                transaction.getType(),
                transaction.getDescription(),
                transaction.getAmount());
    }
}
