package com.ohapps.fitracker.security;

import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CurrentUserService {

    @Autowired
    private UserService userService;

    public User getCurrentUser(HttpServletRequest request) {
        Authentication auth = TokenAuthenticationService.getAuthentication(request);
        return userService.findByEmail(auth.getName());
    }
}
