package com.ohapps.fitracker.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohapps.fitracker.dto.AuthDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

public class TokenAuthenticationService {
    static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "ThisIsASecret";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";

    private TokenAuthenticationService() {
        throw new IllegalStateException("Utility class");
    }

    static void addAuthentication(HttpServletResponse res, String username) throws IOException {
        AuthDto authDto = createAuth(username);
        ObjectMapper objectMapper = new ObjectMapper();
        res.addHeader(HEADER_STRING, authDto.getToken());
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.getWriter().write(objectMapper.writeValueAsString(authDto));
        res.getWriter().flush();
        res.getWriter().close();
    }

    public static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            String user = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            return user != null ?
                    new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList()) :
                    null;
        }
        return null;
    }

    public static String createToken(String username) {
        AuthDto authDto = createAuth(username);
        return TOKEN_PREFIX + " " + authDto.getToken();
    }

    public static AuthDto createAuth(String username){
        Date expiration = new Date(System.currentTimeMillis() + EXPIRATIONTIME);
        String jwt = Jwts.builder()
                .setSubject(username)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
        AuthDto authDto = new AuthDto();
        authDto.setToken(jwt);
        authDto.setExpiration(expiration);
        return authDto;
    }
}
