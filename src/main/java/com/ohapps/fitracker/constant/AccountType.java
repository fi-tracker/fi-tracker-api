package com.ohapps.fitracker.constant;

import java.util.ArrayList;
import java.util.List;

public enum AccountType {

    INVESTMENT("investment"),
    RETIREMENT("retirement"),
    CASHRESERVE("cash reserve");

    private final String type;
    private static final List<String> accountTypes;

    static {
        accountTypes = new ArrayList<>();
        for (AccountType accountType : AccountType.values()) {
            accountTypes.add(accountType.getType());
        }
    }

    AccountType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static List<String> getTypes() {
        return accountTypes;
    }
}
