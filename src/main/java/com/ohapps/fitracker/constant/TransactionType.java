package com.ohapps.fitracker.constant;

import java.util.ArrayList;
import java.util.List;

public enum TransactionType {

    GAIN("gain"),
    GAIN_REINVESTED("gain reinvested"),
    LOSS("loss"),
    LOSS_WITHDRAWN("loss withdrawn from investment"),
    PURCHASE_OF_INVESTMENT("purchase of investment"),
    SALE_OF_INVESTMENT("sale of investment"),
    CHANGE_IN_VALUE("change in value"),
    DEBT_REPAYMENT("debt repayment");

    private final String type;
    private static final List<String> transactionTypes;

    static {
        transactionTypes = new ArrayList<>();
        for (TransactionType transactionType : TransactionType.values()) {
            transactionTypes.add(transactionType.getType());
        }
    }

    TransactionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static List<String> getTypes() {
        return transactionTypes;
    }
}
