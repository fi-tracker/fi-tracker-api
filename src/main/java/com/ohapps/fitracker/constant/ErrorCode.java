package com.ohapps.fitracker.constant;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ErrorCode {

    ACCESS_DENIED("user.access_denied", "access denied"),
    INVALID_INVESTMENT("user.invalid_investment", "invalid investment"),
    INVALID_INVESTMENT_ID("user.invalid_investment_id", "invalid investment id"),
    INVALID_INVESTMENT_TYPE("user.invalid_investment_type", "invalid investment type"),
    INVALID_TRANSACTION_ID("user.invalid_transaction", "invalid transaction id"),
    INVALID_TRANSACTION_TYPE("user.invalid_transaction_type", "invalid transaction type"),
    SYSTEM_TRANSACTION_DELETE("system.transaction_delete", "issue deleting transaction");

    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
