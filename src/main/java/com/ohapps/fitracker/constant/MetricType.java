package com.ohapps.fitracker.constant;

public enum MetricType {
    DOLLAR,
    PERCENT
}
