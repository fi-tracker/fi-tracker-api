package com.ohapps.fitracker.validation;

import com.ohapps.fitracker.constant.ErrorCode;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.SystemError;
import com.ohapps.fitracker.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InvestmentValidator {

    private final AccountService accountService;

    public List<SystemError> isValidInvestment(Investment investment) {
        List<SystemError> errors = new ArrayList<>();

        if (investment == null) {
            errors.add(new SystemError(ErrorCode.INVALID_INVESTMENT));
            return errors;
        }

        if (StringUtils.isEmpty(investment.getType())) {
            errors.add(new SystemError(ErrorCode.INVALID_INVESTMENT_TYPE));
        }

        if (!StringUtils.isEmpty(investment.getAccountId())) {
            accountService.findByIdForUserId(investment.getAccountId(), investment.getUserId());
        }

        return errors;
    }
}
