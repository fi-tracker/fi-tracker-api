package com.ohapps.fitracker.validation;

import com.ohapps.fitracker.entity.User;
import com.ohapps.fitracker.exception.InvalidUserException;
import com.ohapps.fitracker.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Slf4j
public class UserValidator {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void isValid(User user) {
        // validate email
        isValidEmail(user);

        // check if password is set or has changed
        if (user.getPassword() != null && isNewPassword(user)) {

            // validate password meets requirements
            isValidPassword(user.getPassword());

            // Hash password
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
    }

    public void isValidEmail(User user) {
        // unique email check
        User existingUser = userService.findByEmail(user.getEmail());

        if (existingUser != null && !existingUser.getId().equals(user.getId())) {
            throw new InvalidUserException("email is already taken");
        }
    }

    public boolean isNewPassword(User user) {

        boolean newPassword = false;

        if (user.getId() == null) {
            newPassword = true;
        } else {

            User originalUser = userService.findById(user.getId());

            if (originalUser != null && !originalUser.getPassword().equals(user.getPassword())) {
                newPassword = true;
            }
        }

        return newPassword;
    }

    public void isValidPassword(String password) {

        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                // password must be between 8 and 50 characters
                new LengthRule(8, 50),
                // must contain 1 upper case letter
                new UppercaseCharacterRule(1),
                // must contain 1 numeric character
                new DigitCharacterRule(1),
                // must contain 1 special character
                new SpecialCharacterRule(1),
                // must not contain any white spaces
                new WhitespaceRule()));

        RuleResult result = validator.validate(new PasswordData(password));

        if (!result.isValid()) {
            log.info("password validation failed: {}", result.getDetails());
            throw new InvalidUserException("invalid user password");
        }
    }
}
