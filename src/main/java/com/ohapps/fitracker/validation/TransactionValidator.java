package com.ohapps.fitracker.validation;

import com.ohapps.fitracker.constant.ErrorCode;
import com.ohapps.fitracker.constant.TransactionType;
import com.ohapps.fitracker.entity.Investment;
import com.ohapps.fitracker.entity.SystemError;
import com.ohapps.fitracker.entity.Transaction;
import com.ohapps.fitracker.service.InvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionValidator {

    private InvestmentService investmentService;

    @Autowired
    public TransactionValidator(InvestmentService investmentService) {
        this.investmentService = investmentService;
    }

    public List<SystemError> isValidTransaction(Transaction transaction, String userId) {
        List<SystemError> errors = new ArrayList<>();

        if (StringUtils.isEmpty(transaction.getId())) {
            transaction.setId(null);
        }

        isValidUserTransaction(transaction, userId, errors);

        if (!CollectionUtils.isEmpty(errors)) {
            return errors;
        }

        // VALIDATE INDIVIDUAL FIELDS
        if (!TransactionType.getTypes().contains(transaction.getType())) {
            errors.add(new SystemError(ErrorCode.INVALID_TRANSACTION_TYPE));
        }

        return errors;
    }

    public List<SystemError> isValidTransactionForDelete(Transaction transaction, String userId) {
        List<SystemError> errors = new ArrayList<>();

        if (transaction == null) {
            errors.add(new SystemError(ErrorCode.INVALID_INVESTMENT_ID));
            return errors;
        }

        isValidUserTransaction(transaction, userId, errors);

        return errors;
    }

    private void isValidUserTransaction(Transaction transaction, String userId, List<SystemError> errors) {
        Investment investment = investmentService.findById(transaction.getInvestmentId());

        if(investment == null) {
            errors.add(new SystemError(ErrorCode.INVALID_INVESTMENT_ID));
        } else if (!investmentService.isUsersInvestment(investment, userId)) {
            errors.add(new SystemError(ErrorCode.ACCESS_DENIED));
        }
    }
}
